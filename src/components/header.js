import PropTypes from "prop-types"
import React from "react"
import Image from "../components/image"

const Header = ({ siteTitle }) => (
  <header
    style={{
      marginBottom: `1.45rem`,
      maxWidth: `100%`,
    }}
  >
    <div id="headerContent"
        style={{
          maxWidth: `100%`,
          height: `600px`,
        }}
    >
      <div
        style={{
          maxWidth: `70%`,
          padding: `2.0875rem 5.0rem 0.20rem`,
          background: `transparent`,
        }}
      >
          <div style={{ maxWidth: `500px`, marginBottom: `1.45rem` }}>
            <Image />
          </div>



      </div>
      <div id="navigation"
        style={{
          maxWidth: `30%`,
          color: `#FFF`,
          background: `transparent`,
          height: `500px`,

        }}
      >

          <ul id="headerMenu">
            <li>
              <a href="#OurJourney">About fourpercent</a>

            </li>
            <li>
               <a href="#Contact">Contact Us</a>
            </li>
          </ul>

      </div>
    </div>
  </header>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
