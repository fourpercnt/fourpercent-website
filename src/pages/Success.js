import React from "react"
import { Link } from "gatsby"
import Layout from "../components/layout"
import SEO from "../components/seo"

const SecondPage = () => (
  <Layout>

      <SEO title="Form Submission Successful" />
      <h1>Form has been submitted</h1>
      <p>We look forward to your email.</p>
      <Link to="/">Back to home</Link>

  </Layout>
)

export default SecondPage
